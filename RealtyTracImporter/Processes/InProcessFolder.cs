﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace RealtyTracImporter.Processes
{
    public class InProcessFolder
    {
        #region Properties

        private string IncomingFileExtension { get; set; }

        public string Path { get; set; }

        #endregion

        #region Constructors

        public InProcessFolder(string path, string incomingFileExtension)
        {
            IncomingFileExtension = incomingFileExtension;
            Path = path;
            InitFolder();
        }

        #endregion

        #region Methods

        public IEnumerable<InProcessFile> GetInProcessFiles(string[] incomingFiles)
        {
            List<InProcessFile> inprocessFiles = new List<InProcessFile>();
            foreach (string f in incomingFiles)
            {
                string p = String.Format("{0}\\{1}", Path, f.ToLower().Replace(IncomingFileExtension, ".txt"));
                if (!File.Exists(p))
                {
                    inprocessFiles.Add(new InProcessFile(f, Path));
                }                   
            }
            return inprocessFiles;
        }

        public string[] GetFileList(DateTime? minDate = null)
        {
            DirectoryInfo di = new DirectoryInfo(Path);
            if (minDate.HasValue)
                return di.GetFiles().Where(f => f.CreationTimeUtc >= minDate.Value)
                                    .Select(f => f.Name).ToArray();
            else
                return di.GetFiles().Select(f => f.Name).ToArray();
        }

        #endregion

        #region Helpers

        private void InitFolder()
        {
            DirectoryInfo di = new DirectoryInfo(Path);
            if (!di.Exists)
            {
                di.Create();
            }            
        }

        #endregion
    }
}
