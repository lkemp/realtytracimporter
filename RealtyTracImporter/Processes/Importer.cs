﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

using CsvHelper;

using RealtyTracImporter.Audit;
using RealtyTracImporter.Enums;
using RealtyTracImporter.Mapping;
using RealtyTracImporter.Models;

namespace RealtyTracImporter.Processes
{
    public class Importer
    {
        #region Constants

        private const int BULK_SAVE_MAX_SIZE = 1000;

        #endregion

        #region Properties

        public bool AuditEnabled
        {
            get
            {
                string appSetting = ConfigurationManager.AppSettings["AUDIT_ENABLED"];
                bool enabled;
                if (!bool.TryParse(appSetting, out enabled))
                    enabled = false;
                return enabled;
            }
        }

        public string AuditFolderPath
        {
            get { return ConfigurationManager.AppSettings["AUDIT_FOLDER"];  }
        }

        private InProcessFolder InputFolder { get; set; }

        #endregion

        #region Constructors

        public Importer(InProcessFolder inputFolder)
        {
            InputFolder = inputFolder;
        }

        #endregion

        #region Methods

        public void Process(DateTime latestUpdate)
        {
            string[] processFiles = InputFolder.GetFileList(latestUpdate)
                                               .Where(f => !f.Contains(Constants.CHECKSUM_FILE_SUFFIX)).ToArray();
            RawUpdates updates = GetUpdates(processFiles);

            if (AuditEnabled)
            {
                AuditFolder auditFolder = new AuditFolder(AuditFolderPath);
                auditFolder.Clear();

                if (updates.Assessments.Count > 0)
                    AuditAssessments(updates.Assessments.First().Value, auditFolder);

                if (updates.Records.Count > 0)
                    AuditRecords(updates.Records.First().Value, auditFolder);

            }
            else
            {
                foreach (var update in updates.Assessments)
                {
                    ProcessAssessments(update.Value);
                }

                foreach (var update in updates.Equities)
                {
                    ProcessEquities(update.Value);
                }

                foreach (var update in updates.Records)
                {
                    ProcessRecords(update.Value);
                }
            }
        }

        #endregion

        #region Helpers

        private void AuditAssessments(RawUpdateInfo info, AuditFolder folder)
        {
            List<string[]> records = new List<string[]>{
                new AssessorFields().FieldNames
            };

            using (StreamReader reader = File.OpenText(info.DataFileName))
            {
                var csv = GetCsvReader(reader);
                var read = true;
                var counter = 0;
                do
                {
                    read = csv.Read();
                    if (read && csv.CurrentRecord[6].Equals("SAINT JOHNS", StringComparison.OrdinalIgnoreCase))
                    //if (read && !String.IsNullOrEmpty(csv.CurrentRecord[54].Trim()))
                    {
                        records.Add(csv.CurrentRecord);
                        counter++;
                    }

                } while (read && counter < 500);
            }

            string auditFilePath = string.Format("{0}\\Assessments.csv", folder.Path);
            using (StreamWriter writer = new StreamWriter(File.Open(auditFilePath, FileMode.CreateNew)))
            {
                var csv = GetCsvWriter(writer);
                
                foreach (string[] record in records)
                {
                    foreach (string field in record)
                    {
                        csv.WriteField(field);
                    }
                    csv.NextRecord();
                }
                
            }
        }

        private void AuditRecords(RawUpdateInfo info, AuditFolder folder)
        {
            List<string[]> records = new List<string[]>{
                new RecordFields().FieldNames
            };

            using (StreamReader reader = File.OpenText(info.DataFileName))
            {
                var csv = GetCsvReader(reader);
                var read = true;
                var counter = 0;
                do
                {
                    read = csv.Read();
                    if (read && csv.CurrentRecord[7].Equals("St. Johns", StringComparison.OrdinalIgnoreCase))
                    //if (read && !String.IsNullOrEmpty(csv.CurrentRecord[77].Trim()))
                    {
                        records.Add(csv.CurrentRecord);
                        counter++;
                    }

                } while (read && counter < 500);
            }

            string auditFilePath = string.Format("{0}\\Records.csv", folder.Path);
            using (StreamWriter writer = new StreamWriter(File.Open(auditFilePath, FileMode.CreateNew)))
            {
                var csv = GetCsvWriter(writer);

                foreach (string[] record in records)
                {
                    foreach (string field in record)
                    {
                        csv.WriteField(field);
                    }
                    csv.NextRecord();
                }

            }
        }

        private int BulkSave<T>(CsvReader csv) where T : class
        {
            List<T> items = new List<T>();

            int itemCount = 0;

            bool read = true;
            do
            {
                read = csv.Read();

                if (read)
                    items.Add(csv.GetRecord<T>());
                if (items.Count >= BULK_SAVE_MAX_SIZE)
                    read = false;

            } while (read);

            return itemCount;
        }


        private CsvReader GetCsvReader(TextReader reader)
        {
            var csv = new CsvReader(reader);

            csv.Configuration.SkipEmptyRecords = true;
            csv.Configuration.HasHeaderRecord = false;
            csv.Configuration.TrimFields = true;
            csv.Configuration.DetectColumnCountChanges = true;
            csv.Configuration.Delimiter = '\t'.ToString();
            return csv;
        }

        private CsvWriter GetCsvWriter(TextWriter writer)
        {
            var csv = new CsvWriter(writer);

            return csv;
        }

        private RawUpdates GetUpdates(string[] processFiles)
        {
            RawUpdates rawUpdates = new RawUpdates();
            foreach (string f in processFiles)
            {
                string rootName = f.Remove(f.Length - 4);
                string fullName = string.Format("{0}\\{1}", InputFolder.Path, f);
                string checkSum = string.Format("{0}\\{1}{2}.txt", InputFolder.Path, rootName, Constants.CHECKSUM_FILE_SUFFIX);

                FileInfo fi = new FileInfo(fullName);

                string fn = f.ToLower();
                if (fn.Contains("assessor"))
                    rawUpdates.Assessments.Add(fi.CreationTimeUtc, new RawUpdateInfo(fullName, checkSum, UpdateType.Assessments));

                else if (fn.Contains("recorder"))
                    rawUpdates.Records.Add(fi.CreationTimeUtc, new RawUpdateInfo(fullName, checkSum, UpdateType.Records));

                else if (fn.Contains("equity"))
                    rawUpdates.Equities.Add(fi.CreationTimeUtc, new RawUpdateInfo(fullName, checkSum, UpdateType.Equities));               

            }
            return rawUpdates.SortByDate();
        }

        private void ProcessAssessments(RawUpdateInfo info)
        {
            using (StreamReader reader = File.OpenText(info.DataFileName))
            {
                var csv = GetCsvReader(reader);
                csv.Configuration.RegisterClassMap<TaxAssessmentMap>();

                BulkSave<TaxAssessment>(csv);
            }
        }

        private void ProcessEquities(RawUpdateInfo info)
        {
            using (StreamReader reader = File.OpenText(info.DataFileName))
            {
                var csv = GetCsvReader(reader);

                // register class map

                csv.Read();
                string[] fields = csv.CurrentRecord;

                // while (csv.Read())...
            }
        }

        private void ProcessRecords(RawUpdateInfo info)
        {
            using (StreamReader reader = File.OpenText(info.DataFileName))
            {
                var csv = GetCsvReader(reader);

                // register class map

                csv.Read();
                string[] fields = csv.CurrentRecord;
            }
        }

        #endregion
    }
}
