﻿using System;

namespace RealtyTracImporter.Processes
{
    public class InProcessFile
    {
        #region Properties

        public string InProcessPath { get; set; }

        public string IncomingFileName { get; set; }
        
        #endregion

        #region Constructors

        public InProcessFile(string incomingFileName, string inProcessPath)
        {
            IncomingFileName = incomingFileName;
            InProcessPath = inProcessPath;
        }

        #endregion
    }
}
