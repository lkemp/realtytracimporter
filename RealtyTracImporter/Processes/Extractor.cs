﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace RealtyTracImporter.Processes
{
    public static class Extractor
    {
        #region Methods

        public static void Extract(string fileName, string extractPath)
        {
            // Rename checksum file to match data file
            string tempFile = RenameChecksumFile(fileName, extractPath);

            // Extract all contents
            ZipFile.ExtractToDirectory(fileName, extractPath);

            // Clean up
            if (tempFile != null && File.Exists(tempFile))
            {
                File.Delete(tempFile);                
            }
        }

        #endregion

        #region Helpers

        private static bool GetArchiveEntry(ZipArchive archive, string pattern, bool match, out ZipArchiveEntry entry)
        {
            entry = match ? 
                archive.Entries.Where(f => f.Name.ToLower().Contains(pattern.ToLower())).SingleOrDefault() :
                archive.Entries.Where(f => !f.Name.ToLower().Contains(pattern.ToLower())).SingleOrDefault();
            return entry != null; 
        }

        private static string RenameChecksumFile(string fileName, string extractPath)
        {
            string tempFile = null;

            using (FileStream zipToOpen = new FileStream(fileName, FileMode.Open))
            {
                using (ZipArchive archive = new ZipArchive(zipToOpen, ZipArchiveMode.Update))
                {
                    ZipArchiveEntry entry;
                    if (GetArchiveEntry(archive, Constants.CHECKSUM_FILE_SUFFIX, false, out entry))
                    {
                        string rootName = entry.Name.Remove(entry.Name.Length - 4);

                        if (GetArchiveEntry(archive, Constants.CHECKSUM_FILE_SUFFIX, true, out entry))
                        {
                            string assignedName = string.Format("{0}{1}.txt", rootName, Constants.CHECKSUM_FILE_SUFFIX);
                            if (!assignedName.Equals(entry.Name, StringComparison.OrdinalIgnoreCase))
                            {
                                tempFile = string.Format("{0}\\{1}", extractPath, entry.Name);
                                entry.ExtractToFile(string.Format("{0}\\{1}", extractPath, assignedName));
                            }
                        }
                    }
                }
            }

            return tempFile;
        }

        #endregion
    }
}
