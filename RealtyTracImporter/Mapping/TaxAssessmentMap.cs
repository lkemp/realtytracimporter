﻿using System;

using CsvHelper.Configuration;

using RealtyTracImporter.Models;


namespace RealtyTracImporter.Mapping
{
    public class TaxAssessmentMap : CsvClassMap<TaxAssessment>
    {
        #region Constructors

        public TaxAssessmentMap()
        {
            Map(m => m.PropertyID).Index(0);
        }

        #endregion
    }
}
