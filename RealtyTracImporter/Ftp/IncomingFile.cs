﻿using System;

namespace RealtyTracImporter.Ftp
{
    public class IncomingFile
    {
        #region Properties

        public string IncomingPath { get; set; }
        public string OutgoingFileName { get; set; }

        #endregion

        #region Constructors

        public IncomingFile(string outgoingFileName, string incomingPath)
        {
            IncomingPath = incomingPath;
            OutgoingFileName = outgoingFileName;
        }

        #endregion
    }
}
