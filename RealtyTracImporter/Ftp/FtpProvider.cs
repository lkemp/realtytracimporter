﻿using System;
using System.Collections;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Tamir.SharpSsh;

namespace RealtyTracImporter.Ftp
{
    public class FtpProvider
    {
        #region Constants
		
        private const int DEFAULT_PORT = 20;

	    #endregion

        #region Properties

        public string HostName { get; set; }
        public int Port { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        #endregion

        #region Constructors

        public FtpProvider()
        {
            int port;
            HostName = ConfigurationManager.AppSettings["FTP_HOST"];
            Port = int.TryParse(ConfigurationManager.AppSettings["FTP_PORT"], out port) ? port : DEFAULT_PORT;
            UserName = ConfigurationManager.AppSettings["FTP_USERNAME"];
            Password = ConfigurationManager.AppSettings["FTP_PASSWORD"];
        }
        
        #endregion
        
        #region Methods   

        public void GetFile(string directory, string fileName, string downloadPath)
        {
            string filePath = string.Format("/{0}/{1}", directory, fileName);
            Sftp client = GetClient();
            client.Get(filePath, downloadPath);
            client.Close();
        }
     
        public string[] GetFileList(string directory, string fileExtension)
        {
            Sftp client = GetClient();
            ArrayList files = client.GetFileList(directory);
            client.Close();
            return files.ToArray()
                        .Where(f => f.ToString().EndsWith(fileExtension, StringComparison.OrdinalIgnoreCase))
                        .Select(f => f.ToString()).ToArray();
        }


        #endregion

        #region Helpers

        private Sftp GetClient()
        {
            Sftp sftp = new Sftp(HostName, UserName, Password);
            sftp.Connect(Port);
            return sftp;
        }

        #endregion
    }
}
