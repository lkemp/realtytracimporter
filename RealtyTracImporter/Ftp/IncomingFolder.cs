﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace RealtyTracImporter.Ftp
{
    public class IncomingFolder
    {
        #region Properties

        public string Path { get; set; }

        #endregion

        #region Constructors

        public IncomingFolder(string path)
        {
            Path = path;
            InitFolder();
        }

        #endregion

        #region Methods

        public IEnumerable<IncomingFile> GetIncomingFiles(string[] outgoingFiles)
        {
            List<IncomingFile> incomingFiles = new List<IncomingFile>();
            foreach (string f in outgoingFiles)
            {
                string p = String.Format("{0}\\{1}", Path, f);

                if (!File.Exists(p))
                {
                    incomingFiles.Add(new IncomingFile(f, p));
                }                
            }
            return incomingFiles;
        }

        public string[] GetFileList()
        {
            DirectoryInfo di = new DirectoryInfo(Path);
            return di.GetFiles().Select(f => f.Name).ToArray();
        }

        #endregion

        #region Helpers

        private void InitFolder()
        {
            DirectoryInfo di = new DirectoryInfo(Path);
            if (!di.Exists)
            {
                di.Create();
            }            
        }

        #endregion
    }
}
