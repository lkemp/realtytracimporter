﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;

using RealtyTracImporter.Ftp;
using RealtyTracImporter.Processes;

namespace RealtyTracImporter
{
    class Program
    {
        #region Properties

        private static string FtpFileExtension
        {
            get { return ConfigurationManager.AppSettings["FTP_FILE_EXTENSION"];  }
        }

        private static string IncomingFolderPath
        {
            get { return ConfigurationManager.AppSettings["FTP_INCOMING_FOLDER"]; }
        }

        private static string InProcessFolderPath
        {
            get { return ConfigurationManager.AppSettings["IN_PROCESS_FOLDER"];  }
        }

        private static string OutgoingFolderName
        {
            get { return ConfigurationManager.AppSettings["FTP_OUTGOING_FOLDER"]; }
        }

        #endregion

        #region Methods

        static void Main(string[] args)
        {            
            // Download available files
            FtpProvider provider = new FtpProvider();
            string[] outgoingFiles = provider.GetFileList(OutgoingFolderName, FtpFileExtension);

            IncomingFolder incomingFolder = new IncomingFolder(IncomingFolderPath);
            IEnumerable<IncomingFile> incomingFiles = incomingFolder.GetIncomingFiles(outgoingFiles);
            foreach (IncomingFile f in incomingFiles)
            {
                Console.WriteLine("Downloading " + f.OutgoingFileName + " ...");
                DateTime start = DateTime.Now;
                provider.GetFile(OutgoingFolderName, f.OutgoingFileName, f.IncomingPath);
                DateTime end = DateTime.Now;
                Console.WriteLine("    complete: " + GetElapsedTime(start, end));
                Console.WriteLine("");
            }
            
            // Extract downloaded files                       
            InProcessFolder inprocessFolder = new InProcessFolder(InProcessFolderPath, FtpFileExtension);
            string[] newFiles = incomingFolder.GetFileList();
            IEnumerable<InProcessFile> inprocessFiles = inprocessFolder.GetInProcessFiles(newFiles);
            foreach (InProcessFile f in inprocessFiles)
            {
                Console.WriteLine("Extracting " + f.IncomingFileName + " ...");
                DateTime start = DateTime.Now;
                string p = string.Format("{0}\\{1}", incomingFolder.Path, f.IncomingFileName);
                Extractor.Extract(p, f.InProcessPath);
                DateTime end = DateTime.Now;
                Console.WriteLine("    complete: " + GetElapsedTime(start, end));
                Console.WriteLine("");                
            }

            // Process extracted files
            Importer importer = new Importer(inprocessFolder);
            importer.Process(DateTime.UtcNow.AddDays(-30));   

            Console.WriteLine("Press any key to EXIT");
            Console.Read();            
        } 

        #endregion

        #region Helpers

        private static string GetElapsedTime(DateTime start, DateTime end)
        {
            StringBuilder label = new StringBuilder();
            TimeSpan elapsed = end.Subtract(start);
            if (elapsed.Hours > 0)
                label.AppendFormat("{0} hr, {1} min, {2} sec", elapsed.Hours, elapsed.Minutes, elapsed.Seconds);
            else if (elapsed.Minutes > 0)
                label.AppendFormat("{0} min, {1} sec", elapsed.Minutes, elapsed.Seconds);
            else if (elapsed.Seconds > 0)
                label.AppendFormat("{0} sec", elapsed.Seconds);
            else                           
                label.AppendFormat("{0} sec", (double.Parse(elapsed.Milliseconds.ToString()) / 1000).ToString("F2"));
            
            return label.ToString();
        }

        #endregion
    }
}
