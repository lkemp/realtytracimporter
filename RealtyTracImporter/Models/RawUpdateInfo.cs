﻿using System;

using RealtyTracImporter.Enums;

namespace RealtyTracImporter.Models
{
    public class RawUpdateInfo
    {
        #region Properties

        public string DataFileName { get; set; }
        
        public string ChecksumFileName { get; set; }

        public UpdateType UpdateType { get; set; }

        #endregion

        #region Constructors
        
        public RawUpdateInfo(string dataFileName, string checksumFileName, UpdateType updateType)
        {
            DataFileName = dataFileName;
            ChecksumFileName = checksumFileName;
            UpdateType = updateType;
        }

        #endregion
    }

}
