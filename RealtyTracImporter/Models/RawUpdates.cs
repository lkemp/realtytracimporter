﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RealtyTracImporter.Models
{
    public class RawUpdates
    {
        #region Properties

        public IDictionary<DateTime, RawUpdateInfo> Assessments { get; set; }
        public IDictionary<DateTime, RawUpdateInfo> Equities { get; set; }
        public IDictionary<DateTime, RawUpdateInfo> Records { get; set; }

        #endregion

        #region Constructors
        
        public RawUpdates()
        {
            Assessments = new Dictionary<DateTime, RawUpdateInfo>();            
            Equities = new Dictionary<DateTime, RawUpdateInfo>();
            Records = new Dictionary<DateTime, RawUpdateInfo>();
        }

        #endregion

        #region Methods

        public RawUpdates SortByDate()
        {
            Assessments = Assessments.OrderBy(p => p.Key)
                                     .ToDictionary(k => k.Key, v => v.Value);
            
            Equities = Equities.OrderBy(p => p.Key)
                               .ToDictionary(k => k.Key, v => v.Value);
            
            Records = Records.OrderBy(p => p.Key)
                             .ToDictionary(k => k.Key, v => v.Value);

            return this;
        }

        #endregion
    }
}
