﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RealtyTracImporter.Models
{
    public class RecordFields
    {
        #region Properties

        public string[] FieldNames
        {
            get
            {
                List<string> fieldNames = new List<string> {
                    "SR_UNIQUE_ID"
                    ,"SR_PROPERTY_ID"
                    ,"SR_SCM_ID"
                    ,"MM_STATE_CODE"
                    ,"MM_MUNI_NAME"
                    ,"MM_FIPS_STATE_CODE"
                    ,"MM_FIPS_MUNI_CODE"
                    ,"MM_FIPS_COUNTY_NAME"
                    ,"SR_PARCEL_NBR_RAW"
                    ,"SR_SITE_ADDR_RAW"
                    ,"SR_MAIL_ADDR_RAW"
                    ,"SR_MAIL_HOUSE_NBR"
                    ,"SR_MAIL_FRACTION"
                    ,"SR_MAIL_DIR"
                    ,"SR_MAIL_STREET_NAME"
                    ,"SR_MAIL_SUF"
                    ,"SR_MAIL_POST_DIR"
                    ,"SR_MAIL_UNIT_PRE"
                    ,"SR_MAIL_UNIT_VAL"
                    ,"SR_MAIL_CITY"
                    ,"SR_MAIL_STATE"
                    ,"SR_MAIL_ZIP"
                    ,"SR_MAIL_PLUS_4"
                    ,"SR_MAIL_CRRT"
                    ,"SR_LGL_DSCRPTN"
                    ,"SR_LGL_KEYED_BLOCK"
                    ,"SR_LGL_KEYED_LOT"
                    ,"SR_LGL_KEYED_PLAT_BOOK"
                    ,"SR_LGL_KEYED_PLAT_PAGE"
                    ,"SR_LGL_KEYED_RANGE"
                    ,"SR_LGL_KEYED_SECTION"
                    ,"SR_LGL_KEYED_SUB_NAME"
                    ,"SR_LGL_KEYED_TOWNSHIP"
                    ,"SR_LGL_KEYED_TRACT"
                    ,"SR_LGL_KEYED_UNIT"
                    ,"SR_BUYER"
                    ,"SR_SELLER"
                    ,"SR_VAL_TRANSFER"
                    ,"SR_TAX_TRANSFER"
                    ,"SR_DOC_NBR_RAW"
                    ,"SR_DOC_NBR_FMT"
                    ,"SR_DATE_TRANSFER"
                    ,"SR_DATE_FILING"
                    ,"SR_DOC_TYPE"
                    ,"SR_DEED_TYPE"
                    ,"SR_TRAN_TYPE"
                    ,"SR_QUITCLAIM"
                    ,"SR_ARMS_LENGTH_FLAG"
                    ,"SR_FULL_PART_CODE"
                    ,"SR_MULT_APN_FLAG_KEYED"
                    ,"SR_MULT_PORT_CODE"
                    ,"SR_LNDR_SELLER_FLAG"
                    ,"SR_TD_DOC_NBR_1"
                    ,"SR_LOAN_ID_1"
                    ,"SR_LOAN_ID_1_EXT"
                    ,"SR_LOAN_VAL_1"
                    ,"SR_LOAN_TYPE_1"
                    ,"SR_INT_RATE_TYPE_1"
                    ,"SR_LNDR_CREDIT_LINE_1"
                    ,"SR_LNDR_CODE_1"
                    ,"SR_LNDR_FIRST_NAME_1"
                    ,"SR_LNDR_LAST_NAME_1"
                    ,"SR_LENDER_TYPE_1"
                    ,"SR_TD_DOC_NBR_2"
                    ,"SR_LOAN_ID_2"
                    ,"SR_LOAN_ID_2_EXT"
                    ,"SR_LOAN_VAL_2"
                    ,"SR_LOAN_TYPE_2"
                    ,"SR_INT_RATE_TYPE_2"
                    ,"SR_LNDR_CREDIT_LINE_2"
                    ,"SR_LNDR_CODE_2"
                    ,"SR_LNDR_FIRST_NAME_2"
                    ,"SR_LNDR_LAST_NAME_2"
                    ,"SR_LENDER_TYPE_2"
                    ,"SR_TD_DOC_NBR_3"
                    ,"SR_LOAN_ID_3"
                    ,"SR_LOAN_ID_3_EXT"
                    ,"SR_LOAN_VAL_3"
                    ,"SR_LOAN_TYPE_3"
                    ,"SR_INT_RATE_TYPE_3"
                    ,"SR_LNDR_CODE_3"
                    ,"SR_LNDR_CREDIT_LINE_3"
                    ,"SR_LNDR_FIRST_NAME_3"
                    ,"SR_LNDR_LAST_NAME_3"
                    ,"SR_LENDER_TYPE_3"
                    ,"DISTRESS_INDICATOR"
                    ,"PROCESS_ID"
                    ,"FILLER"
                };
                return fieldNames.ToArray();
            }
        }

        #endregion
    }
}
