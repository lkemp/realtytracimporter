﻿using System;

namespace RealtyTracImporter.Enums
{
    public enum UpdateType
    {
        Undefined = -1,
        Assessments = 0,
        Equities = 1,
        Records = 2
    }
}
